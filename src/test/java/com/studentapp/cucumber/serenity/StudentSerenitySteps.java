package com.studentapp.cucumber.serenity;

import com.studentapp.model.StudentClass;
import com.studentapp.utils.ReusableSpecifications;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.HashMap;
import java.util.List;

public class StudentSerenitySteps {

    private StudentClass student = new StudentClass();

    private StudentClass setStudentInfo(String firstName, String lastName, String email, String programme, List<String> courses) {
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setProgramme(programme);
        student.setEmail(email);
        student.setCourses(courses);

        return student;
    }

    @Step("Creating a student info with firstName: {0}, lastName: {1}, email: {2}, programme: {3}, courses: {4}")
    public ValidatableResponse createStudent(String firstName, String lastName, String email, String programme, List<String> courses) {
        setStudentInfo(firstName, lastName, email, programme, courses);
        return SerenityRest
                .given().spec(ReusableSpecifications.getGenericRequestSpec())
                .when().body(student).post()
                .then().spec(ReusableSpecifications.getGenericResponseSpec());
    }

    @Step("Getting the Student info by first name: {0}")
    public HashMap<String, Object> getStudentInfoByFirstName(String firstName) {
        String p1 = "findAll{it.firstName=='";
        String p2 = "'}.get(0)";

        return SerenityRest.rest().given()
                .when().get("/list")
                .then().log().all().statusCode(200)
                .extract().path(p1+firstName+p2);
    }

    @Step("Updating student info with studentID: {0}, firstName:{1}, lastName:{2}, email:{3}, programme:{4}, courses:{5}")
    public ValidatableResponse updateStudent(int studentId, String firstName, String lastName, String email, String programme, List<String> courses) {
        setStudentInfo(firstName, lastName, email, programme, courses);

        return SerenityRest.rest().given().spec(ReusableSpecifications.getGenericRequestSpec()).log().all()
                .when().body(student).put("/" + studentId)
                .then().spec(ReusableSpecifications.getGenericResponseSpec());
    }

    @Step("Deleting the student info with ID: {0}")
    public Response deleteStudent(int studentId) {
        return SerenityRest.rest().given().when().delete("/"+studentId);
    }

    @Step("Getting the Student info by student ID: {0}")
    public ValidatableResponse getStudentById(int studentId) {
        return SerenityRest.rest().given()
                .when().get("/"+studentId)
                .then().spec(ReusableSpecifications.getGenericResponseSpec());
    }
}

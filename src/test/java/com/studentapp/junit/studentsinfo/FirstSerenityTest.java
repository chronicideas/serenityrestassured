package com.studentapp.junit.studentsinfo;

import com.studentapp.testbase.TestBase;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Manual;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Title;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

@RunWith(SerenityRunner.class)
public class FirstSerenityTest extends TestBase {

    @Test
    public void testGetAllStudents_Success() {
        SerenityRest.given()
                .when()
                .get("/list")
                .then()
                .log()
                .all()
                .statusCode(200);
    }

    @Test
    public void testGetAllStudents_Fail() {
        SerenityRest.given()
                .when()
                .get("/list")
                .then()
                .log()
                .all()
                .statusCode(500);
    }

    @Test
    @Pending
    public void testGetAllStudents_Pending() {

    }

    @Test
    @Ignore
    public void testGetAllStudents_Skip() {

    }

    @Test
    public void testGetAllStudents_Error() {
        System.out.println("This is an error"+(5/0));
    }

    @Test
    public void testGetAllStudents_Compromisable() throws FileNotFoundException {
        File file = new File("src/tom.txt");
        FileReader fileReader = new FileReader(file);
    }

    @Test
    @Manual
    public void testGetAllStudents_Manual() {

    }

    @Test
    @Title("This test will get a list of all the students from the StudentApp API")
    public void testGetAllStudents_NiceTestTitle() {
        SerenityRest.given()
                .when()
                .get("/list")
                .then()
                .log()
                .all()
                .statusCode(200);
    }
}

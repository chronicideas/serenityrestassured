package com.studentapp.junit.studentsinfo;

import com.studentapp.cucumber.serenity.StudentSerenitySteps;
import com.studentapp.testbase.TestBase;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.Concurrent;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

@Concurrent(threads = "4x")
@UseTestDataFrom("testdata/studentinformation.csv")
@RunWith(SerenityParameterizedRunner.class)
public class CreateStudentsDataDriventest extends TestBase {

//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getProgramme() {
//        return programme;
//    }
//
//    public void setProgramme(String programme) {
//        this.programme = programme;
//    }
//
//    public String getCourse() {
//        return course;
//    }
//
//    public void setCourses(String course) {
//        this.course = course;
//    }
//
//    public StudentSerenitySteps getSteps() {
//        return steps;
//    }
//
//    public void setSteps(StudentSerenitySteps steps) {
//        this.steps = steps;
//    }

    private String firstName;
    private String lastName;
    private String email;
    private String programme;
    private String course;

    @Steps
    private StudentSerenitySteps steps;

    @Test
    @Title("DataDriven test for adding multiple students to the student app") //using csv
    public void testCreateMultipleStudents_Success() {
        List<String> courses = new ArrayList<>();
        courses.add(course);

        steps.createStudent(firstName, lastName, email, programme, courses)
        .statusCode(201);
    }
}

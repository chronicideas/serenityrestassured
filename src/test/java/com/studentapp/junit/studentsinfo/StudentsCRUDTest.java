package com.studentapp.junit.studentsinfo;

import com.studentapp.cucumber.serenity.StudentSerenitySteps;
import com.studentapp.model.StudentClass;
import com.studentapp.testbase.TestBase;
import com.studentapp.utils.ReusableSpecifications;
import com.studentapp.utils.TestUtils;
import io.restassured.http.ContentType;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SerenityRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StudentsCRUDTest extends TestBase {

    private static String firstName = "Thomas" + TestUtils.getRandomValue();
    private static String lastName = "Knee";
    private static String email = TestUtils.getRandomValue() + "tom@testifyqa.com";
    private static String programme = "Computer Science";
    private static int studentID;

    @Steps
    private StudentSerenitySteps studentSerenitySteps;

    private List<String> getCourses() {
        List<String> courses = new ArrayList<>();
        courses.add("JAVA");
        courses.add("C++");
        return courses;
    }

    @Test
    @Title("Create a new student")
    public void test001_CreateStudent_Success() {
        studentSerenitySteps.createStudent(firstName, lastName, email, programme, getCourses()).statusCode(201)
        .spec(ReusableSpecifications.getGenericResponseSpec());
    }

    @Test
    @Title("Check newly created student was added to the list")
    public void test002_GetStudent_ContainsCreatedStudents() {
        HashMap<String, Object> value = studentSerenitySteps.getStudentInfoByFirstName(firstName);
        assertThat(value, hasValue(firstName));
        studentID = (int) value.get("id");
    }

    @Test
    @Title("Update a created student with new details")
    public void test003_UpdateStudent_Success() {
        firstName = firstName+"_Updated";
        studentSerenitySteps.updateStudent(studentID, firstName, lastName, email, programme, getCourses());
        HashMap<String, Object> value = studentSerenitySteps.getStudentInfoByFirstName(firstName);
        assertThat(value, hasValue(firstName));
    }

    @Test
    @Title("Delete the student and verify it is deleted")
    public void test004_DeleteStudent() {
        studentSerenitySteps.deleteStudent(studentID);
        studentSerenitySteps.getStudentById(studentID);
    }
}

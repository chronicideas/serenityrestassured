package com.studentapp.junit.studentsidinfo;

import io.restassured.RestAssured;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Manual;
import net.thucydides.core.annotations.Pending;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

@RunWith(SerenityRunner.class)
public class SerenityIdInfo {

    @BeforeClass
    public static void init() {
        RestAssured.baseURI = "http://localhost:8085/student";
    }

    @Test
    public void testGetAllStudents_Success() {
        SerenityRest.given()
                .when()
                .get("/list")
                .then()
                .log()
                .all()
                .statusCode(200);
    }

    @Test
    public void testGetAllStudents_Fail() {
        SerenityRest.given()
                .when()
                .get("/list")
                .then()
                .log()
                .all()
                .statusCode(500);
    }

    @Test
    @Pending
    public void testGetAllStudents_Pending() {

    }

    @Test
    @Ignore
    public void testGetAllStudents_Skip() {

    }

    @Test
    public void testGetAllStudents_Error() {
        System.out.println("This is an error"+(5/0));
    }

    @Test
    public void testGetAllStudents_Compromisable() throws FileNotFoundException {
        File file = new File("src/tom.txt");
        FileReader fileReader = new FileReader(file);
    }

    @Test
    @Manual
    public void testGetAllStudents_Manual() {

    }
}
